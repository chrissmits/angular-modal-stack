# Angular Modal Stack

Ability to stack bootstrap 4 modals on top of each other and communicate between the modals in a event driven way.

## Usage

Add the component you wish to use as a modal to the ENTRY_MODALS in the app.module.ts file.

```typescript
const ENTRY_MODALS = {
  key: Component
};
```

To open a modal use the following approach.

```typescript
this.modalStackService.createModal('ENTRY_MODALS.key', {
    key: 'value'
}, {
    save: this.onSave.bind(this), // Called when modal is saved
    close: this.onClose.bind(this) // Called when modal closes
}, 'classes');
```

## Flow

### Inputs

Page > Modal > Modal > Modal > ...

### Outputs

Page < Modal < Modal < Modal < ...
