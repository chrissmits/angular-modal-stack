import { Injectable, ComponentFactoryResolver, ViewContainerRef, Type, ComponentRef, InjectionToken, Inject } from '@angular/core';
import { ModalWrapperComponent } from '../components/modal-wrapper/modal-wrapper.component';

export const MODALS = new InjectionToken<{[key: string]: Type<any>}>('Entry component modals');

@Injectable({
  providedIn: 'root'
})
export class ModalStackService {

  protected modalContainerRef: ViewContainerRef;
  protected modals: {[key: string]: Type<any>};

  constructor(private componentFactoryResolver: ComponentFactoryResolver, @Inject(MODALS) modals: {[key: string]: Type<any>}) {
    this.modals = modals;
  }

  setModalContainerRef(modalContainerRef: ViewContainerRef) {
    this.modalContainerRef = modalContainerRef;
  }

  createModal(modal: string, data: {}, actions: {}, classes?: string) {
    let component: Type<any>;
    if (this.modals.hasOwnProperty(modal)) {
      component = this.modals[modal];
    } else {
      throw Error('Modal is not registered, did you forget to add it to ENTRY_MODALS in app.module.ts?');
    }

    if (this.modalContainerRef.length === 0) {
      this.createModalBackdrop();
    } else {
      this.updateModalBackdropLevel(this.modalContainerRef.length);
    }

    const modalWrapperRef = this.createModalWrapper(classes, this.modalContainerRef.length);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = modalWrapperRef.instance.modalWrapper.viewContainerRef.createComponent(componentFactory);

    // Add some default properties (mandatory in modalized components)
    data = Object.assign(data, {
      isModal: true,
    });

    // Set each property in the data argument separately
    this.setDataProperties(componentRef, data);

    // Set each action in the actions argument separately
    this.setActions(componentRef, modalWrapperRef, actions);

    // Make sure that angular processes any changes in the modal wrapper
    modalWrapperRef.changeDetectorRef.detectChanges();

    // Make sure that angular processes any changes in the modal
    componentRef.changeDetectorRef.detectChanges();
  }

  private setDataProperties(componentRef: ComponentRef<any>, data: {}) {
    for (const key in data) {
      componentRef.instance[key] = data[key];
    }
  }

  private setActions(componentRef: ComponentRef<any>, modalWrapperRef: ComponentRef<ModalWrapperComponent>, actions: {}) {
    for (const key in actions) {
      if (componentRef.instance.hasOwnProperty(key)) {
        // When you emit an output in the modal it is catched here and executed in the context of the parent component
        componentRef.instance[key].subscribe(((event$: any) => {
          componentRef.destroy();
          modalWrapperRef.destroy();
          actions[key](event$);
          if (this.modalContainerRef.length === 0) {
            this.removeModalBackdrop();
          } else {
            this.updateModalBackdropLevel(this.modalContainerRef.length - 1);
          }
        }));
      }
    }
  }

  private createModalWrapper(classes: string, level: number): ComponentRef<ModalWrapperComponent> {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ModalWrapperComponent);
    const componentRef = this.modalContainerRef.createComponent(componentFactory);
    componentRef.instance.classes = classes;
    componentRef.instance.level = level;
    return componentRef;
  }

  private createModalBackdrop() {
    const backdrop = document.createElement('div');
    backdrop.className = 'modal-backdrop fade show';
    document.querySelector('body').appendChild(backdrop);
  }

  private updateModalBackdropLevel(level: number) {
    (document.querySelector('.modal-backdrop') as HTMLElement).style.zIndex = (1040 + (level * 20)).toString();
  }

  private removeModalBackdrop() {
    document.querySelector('.modal-backdrop').remove();
  }

}
