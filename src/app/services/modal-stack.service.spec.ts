import { TestBed } from '@angular/core/testing';

import { ModalStackService } from './modal-stack.service';

describe('ModalStackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalStackService = TestBed.get(ModalStackService);
    expect(service).toBeTruthy();
  });
});
