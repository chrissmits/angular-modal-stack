import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DynamicComponentDirective } from './directives/dynamic-component.directive';
import { ModalWrapperComponent } from './components/modal-wrapper/modal-wrapper.component';
import { ListComponent } from './components/list/list.component';
import { EditComponent } from './components/edit/edit.component';
import { MODALS } from './services/modal-stack.service';

const ENTRY_MODALS = {
  list: ListComponent,
  edit: EditComponent
};

@NgModule({
  declarations: [
    AppComponent,
    DynamicComponentDirective,
    ModalWrapperComponent,
    ListComponent,
    EditComponent
  ],
  entryComponents: [
    ModalWrapperComponent,
    ListComponent,
    EditComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {
      provide: MODALS,
      useValue: ENTRY_MODALS
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
