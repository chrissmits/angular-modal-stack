import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicComponentDirective } from './directives/dynamic-component.directive';
import { ModalStackService } from './services/modal-stack.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild(DynamicComponentDirective) modalContainer: DynamicComponentDirective;

  constructor(private modalStackService: ModalStackService) {}

  ngOnInit(): void {
    this.modalStackService.setModalContainerRef(this.modalContainer.viewContainerRef);
  }

  openModal() {
    this.modalStackService.createModal('list', {
      message: 'This message is shown in the modal'
    }, {
      save: this.onSave.bind(this),
      close: this.onClose.bind(this)
    }, 'mw-100 w-75');
  }

  onSave(event: any): void {
  }

  onClose(event: any): void {
  }

}
