import { Component, Input, ViewChild } from '@angular/core';
import { DynamicComponentDirective } from '../../directives/dynamic-component.directive';

@Component({
  selector: 'app-modal-wrapper',
  template: `
    <div class="modal fade show" [style.z-index]="1050 + (level * 20)" [style.display]="'block'">
      <div class="modal-dialog" [ngClass]="classes">
        <div class="modal-content">
          <div appDynamicComponent></div>
        </div>
      </div>
    </div>
  `
})
export class ModalWrapperComponent {

  @ViewChild(DynamicComponentDirective) modalWrapper: DynamicComponentDirective;

  @Input() classes = '';
  @Input() level = 0;

}
