import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModalStackService } from '../../services/modal-stack.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(private modalStackService: ModalStackService) { }

  ngOnInit() {
  }

  edit() {
    this.modalStackService.createModal('edit', {}, {
      save: this.onSave.bind(this),
      close: this.onClose.bind(this)
    });
  }

  onSave(event: any) {
  }

  onClose() {
  }

  saveModal() {
    this.save.emit();
  }

  closeModal() {
    this.close.emit();
  }

}
